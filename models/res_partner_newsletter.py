# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields


class ResPartnerNewsletterSubscription(models.Model):
    _name = "res.partner.newsletter.subscription"
    _description = "Subscription"

    partner_id = fields.Many2one(
        comodel_name='res.partner',
        string='Contact',
        domain=[('is_company', '=', False)],
        ondelete='cascade', index=True)
    newsletter_id = fields.Many2one(
        comodel_name='res.partner.newsletter',
        string='Newsletter',
        domain=[('is_company', '=', False)],
        ondelete='cascade', index=True)
    consent = fields.Boolean("Consentement")


class ResPartnerNewsletter(models.Model):
    _name = "res.partner.newsletter"
    _description = "Newsletter"

    name = fields.Char('Newsletter')
    id_riga = fields.Integer("Identifiant RIGA")
