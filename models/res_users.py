# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import fields, models, api, modules


class ResUsers(models.Model):
    _inherit = "res.users"

    @api.model
    def _get_ur(self):
        return self.env.user.company_id.ur_id

    ur_id = fields.Many2one('union.regionale', compute='_compute_ur_id',
                            string='UR', store=True)

    @api.depends('company_id.ur_id')
    def _compute_ur_id(self):
        for user in self:
            user.ur_id = user.company_id.ur_id

    @api.model
    def systray_get_activities(self):
        activities = super(ResUsers, self).systray_get_activities()
        for activity in activities:
            if activity['model'] != 'res.partner':
                continue
            activity['icon'] = modules.module.get_module_icon('contacts')
            activity['actions'] = [{
                'icon': 'fa-th',
                'name': 'Summary',
                'action_xmlid': 'cgscop_partner.cgscop_action_contacts_summary'
            }]
        return activities

