# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields


class ScopContributions(models.Model):
    _name = "scop.contribution"
    _description = "Contributions"
    _order = 'partner_id, year, type_id'

    partner_id = fields.Many2one(
        comodel_name='res.partner',
        string='Organisme',
        domain=[('is_company', '=', True)],
        ondelete='cascade', index=True)
    id_riga = fields.Integer("ID RIGA")
    type_id = fields.Many2one(
        'scop.contribution.type',
        string="Type de cotisation",
        ondelete='restrict', index=True)
    year = fields.Integer("Année", index=True)
    calculation_date = fields.Datetime("Date calcul cotisation")
    loss = fields.Boolean("Perte")
    amount_calculated = fields.Integer("Montant calculé")
    amount_called = fields.Integer("Montant appelé")
    amount_paid = fields.Float("Montant réglé")
    amount_remaining_previous = fields.Float("Mnt Restant cum. à fin N-1")
    amount_paid_late = fields.Float("Mnt réglé après clôture")
    spreading = fields.Integer("Echelonnement")
    amount_remaining = fields.Float("Montant Restant")
    quarter_1 = fields.Date("1er Trim.")
    quarter_2 = fields.Date("2ème Trim.")
    quarter_3 = fields.Date("3ème Trim.")
    quarter_4 = fields.Date("4ème Trim.")


class ScopContributionType(models.Model):
    _name = "scop.contribution.type"
    _description = "Contribution type"

    name = fields.Char('Contribution type')
    id_riga = fields.Integer("ID RIGA")
