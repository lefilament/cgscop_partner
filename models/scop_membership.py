# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields


class ScopMembershipPeriod(models.Model):
    _name = "scop.membership.period"
    _description = "Membership period"
    _order = "start desc"

    partner_id = fields.Many2one(
        comodel_name='res.partner',
        string='Organisme',
        domain=[('is_company', '=', True)],
        ondelete='cascade', index=True)
    id_riga = fields.Integer("ID RIGA")
    type_id = fields.Many2one(
        'scop.membership.type',
        string="Type d'adhésion",
        ondelete='restrict', index=True)
    start = fields.Date('Début d’adhésion', index=True)
    end = fields.Date('Fin d’adhésion')
    end_reason_id = fields.Many2one(
        'scop.membership.reason.end',
        string='Motif de fin d’adhésion',
        ondelete='restrict')
    number = fields.Char("No adhérent", index=True)
    state = fields.Selection(
        [("none", "Non Adhérent"),
         ("ongoing", "Traitement de l'adhésion"),
         ("approval", "Attente d'approbation"),
         ("done", "Adhérent")],
        string="Statut adhésion", default="none")
    note = fields.Text('Commentaires')


class ScopMembershipType(models.Model):
    _name = "scop.membership.type"
    _description = "Membership type"

    name = fields.Char('Membership type')


class ScopMembershipReasonEnd(models.Model):
    _name = "scop.membership.reason.end"
    _description = "Reason for end of membership"

    name = fields.Char('Reason for end of membership')
