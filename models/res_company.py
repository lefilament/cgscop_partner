# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import fields, models, api


class ResCompany(models.Model):
    _inherit = "res.company"

    @api.model
    def _ur_default_get(self):
        # Renvoie l'UR de l'utlisateur courant
        return self.env['res.users']._get_ur()

    ur_id = fields.Many2one('union.regionale', string='UR')
