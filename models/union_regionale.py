# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import fields, models


class UnionRegionale(models.Model):
    _name = 'union.regionale'
    _description = 'Union Régionale'

    long_name = fields.Char("Nom", required=True, index=True)
    name = fields.Char("Abbréviation", required=True, index=True)
    id_riga = fields.Integer("Identifiant RIGA", index=True)

    _sql_constraints = [('name_uniq', 'unique (name)',
                         "Cette Union Régionale existe déjà !")]
