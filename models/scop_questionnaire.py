# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from datetime import date
from odoo import models, fields, api


class ScopQuestionnaire(models.Model):
    _name = "scop.questionnaire"
    _description = "Questionnaire"

    partner_id = fields.Many2one(
        comodel_name='res.partner',
        string='Organisme',
        domain=[('is_company', '=', True)],
        ondelete='cascade', index=True)
    id_riga = fields.Integer("ID RIGA")
    year = fields.Char("Année", index=True)
    type_id = fields.Many2one(
        'scop.questionnaire.type',
        string="Type de questionnaire",
        ondelete='restrict', index=True)
    effective_date = fields.Date("Date d'effet du questionnaire")
    staff_count = fields.Integer("Effectif (EF)")
    staff_shareholder_count = fields.Integer("Eff. Sociétaires (ES)")
    staff_average = fields.Integer("Eff. Moyen (EM)")


class ScopQuestionnaireType(models.Model):
    _name = "scop.questionnaire.type"
    _description = "Type de Questionnaire"

    name = fields.Char('Type de Questionnaire')
