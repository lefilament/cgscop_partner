# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields


class ScopRevision(models.Model):
    _name = "scop.revision"
    _description = "Revision"
    _order = 'partner_id, date'

    partner_id = fields.Many2one(
        comodel_name='res.partner',
        string='Organisme',
        domain=[('is_company', '=', True)],
        ondelete='cascade', index=True)
    date = fields.Date("Date de révision", index=True)
    delegate_id = fields.Many2one(
        'res.users',
        string='Réviseur',
        ondelete='restrict')
    revision_result_year = fields.Integer("Exercice révisé")
    revision_staff = fields.Integer("Effectif")
    revision_format_id = fields.Many2one(
        'scop.revision.format',
        string='Format de révision',
        on_delete='restrict',
        related='partner_id.revision_format_id',
        store=True)
    revision_followup = fields.Selection(
        [('ras', "RAS"),
         ('remarks', 'Simples Remarques et Observations'),
         ('reserve', "Réserves"),
         ('corrections', 'Mesures Correctives'),
         ('demeure', 'Mise en demeure')],
        string='Suivi après révision')
    note = fields.Text("Commentaire")


class ScopRevisionFormat(models.Model):
    _name = "scop.revision.format"
    _description = "Revision Format"
    _order = 'ur_id, name'

    def _default_ur(self):
        return self.env['res.company']._ur_default_get()

    name = fields.Char('Revision Format')
    ur_id = fields.Many2one(
        'union.regionale',
        string='Union Régionale',
        index=True,
        on_delete='restrict',
        default=_default_ur)
