# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields, api


class ScopPeriod(models.Model):
    _name = "scop.period"
    _description = "SCOP Period"
    _order = "partner_id, start desc"

    # Infos générales
    partner_id = fields.Many2one(
        comodel_name='res.partner',
        string='Organisme',
        domain=[('is_cooperative', '=', True)],
        ondelete='cascade', required=True, index=True)
    id_riga = fields.Integer("ID RIGA")
    start = fields.Date('Début de validité', required=True,
                        default=fields.Date.today(), index=True)
    end = fields.Date('Fin de validité')
    end_reason = fields.Selection(
        [('juri', "Modification de la forme juridique"),
         ('form', "Changement de forme coopérative"),
         ('acti', "Changement d'activité (NAF)"),
         ('adr', "Changement d'adresse"),
         ('nom', "Changement de dénomination sociale"),
         ('deces', 'Décès'),
         ('autr', "Autres")],
        string='Motif de fin de validité')
    comments = fields.Text('Commentaires')
    name = fields.Char('Raison Sociale', required=True, index=True)
    cooperative_form_id = fields.Many2one(
        'res.partner.cooperative.form',
        string="Cooperative form",
        on_delete='restrict')
    partner_company_type_id = fields.Many2one(
        comodel_name='res.partner.company.type',
        string='Legal Form',
        track_visibility='onchange',
        on_delete='restrict')
    siret = fields.Char(string='SIRET', size=14)
    street = fields.Char()
    street2 = fields.Char()
    street3 = fields.Char()
    zip = fields.Char()
    zip_id = fields.Many2one('res.city.zip', 'ZIP Location')
    city = fields.Char()
    cedex = fields.Char()
    state_id = fields.Many2one(
        "res.country.state",
        string='State',
        ondelete='restrict',
        domain="[('country_id', '=?', country_id)]")
    country_id = fields.Many2one(
        'res.country', string='Country', ondelete='restrict')
    naf_id = fields.Many2one(
        'res.partner.naf',
        string='Code NAF',
        on_delete='restrict')
    cae = fields.Boolean("CAE")
    dissolution_reason_id = fields.Many2one(
        'res.partner.dissolution.reason',
        string="Motif Décés",
        on_delete='restrict',
        track_visibility='onchange')

    @api.multi
    def write(self, vals):
        for period in self:
            partner_vals = dict(vals)
            partner_vals.pop('partner_id', False)
            partner_vals.pop('id_riga', False)
            partner_vals.pop('start', False)
            partner_vals.pop('end', False)
            partner_vals.pop('end_reason', False)
            partner_vals.pop('comments', False)
            partner_vals.pop('dissolution_reason_id', False)
            # Update partner
            period.partner_id.write(partner_vals)
        return super(ScopPeriod, self).write(vals)
