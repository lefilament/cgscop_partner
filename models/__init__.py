# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from . import res_company
from . import mail_activity_type
from . import res_partner
from . import res_partner_newsletter
from . import res_users
from . import scop_contribution
from . import scop_liasse_fiscale
from . import scop_membership
from . import scop_period
from . import scop_questionnaire
from . import scop_revision
from . import union_regionale
