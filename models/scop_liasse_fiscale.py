# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields


class ScopLiasseFiscale(models.Model):
    _name = "scop.liasse.fiscale"
    _description = "Liasse Fiscale"

    id_riga = fields.Integer("ID RIGA", index=True)
    partner_id = fields.Many2one(
        comodel_name='res.partner',
        string='Organisme',
        domain=[('is_company', '=', True)],
        ondelete='cascade', index=True)
    year = fields.Char("Année", index=True)
    type_id = fields.Many2one(
        'scop.liasse.fiscale.type',
        string="Type de Liasse Fiscale",
        ondelete='restrict', index=True)
    effective_date = fields.Date("Date d'effet de la liasse")
    duration = fields.Integer("Durée de l’exercice")
    closing_date = fields.Date("Exercice clos le")
    source_id = fields.Many2one(
        'scop.liasse.fiscale.source',
        string="Source de Liasse Fiscale",
        ondelete='restrict')
    revenue_cg = fields.Float("'CA' sens CGSCOP")
    margin_cg = fields.Float("'Marge' sens CGSCOP")
    av_cg = fields.Float("'VABDF' sens CGSCOP")
    wage_cg = fields.Float("'Salaires' sens CGSCOP")
    margin2ca = fields.Float("Marge*2 > CA")
    margin_cgsubv = fields.Float("'Marge' sens CGSCOP-Subv")
    av_cgsubv = fields.Float("'VA BDF' sens CGSCOP-Subv")
    revenue_cgsubv = fields.Float("'CA' sens CGSCOP-Subv")
    is_av_lf = fields.Boolean("VA sur LF")
    av_lf = fields.Float("VA indiquée sur LF")
    revenue_sub = fields.Float("CA_SUB")
    sal = fields.Float("SAL")
    margin = fields.Float("MARGE")
    distribution = fields.Float(
        "Répartition statutaire des excédents nets de gestion")
    capital_cae = fields.Float("Capital détenu (CAE)")
    flexible_keys = fields.Boolean("Clés flexible")
    reserve = fields.Float("Réserve légale")
    share_ass_perm = fields.Float("Part des permanents associés")
    dvpt_fund = fields.Float("Fond de développement")
    share_ass_work = fields.Float("Part des entrepreneurs salariés associés")
    share_capital = fields.Float("Part capital")
    share_work = fields.Float("Part travail")
    pension_fund = fields.Float("Caisse retraite/solidarité")
    others = fields.Float("Autres")
    key_name = fields.Float("Nom de la clé")
    status_update = fields.Date("Modification des statuts")


class ScopLiasseFiscaleType(models.Model):
    _name = "scop.liasse.fiscale.type"
    _description = "Type de Liasse Fiscale"

    name = fields.Char('Type de Liasse Fiscale')


class ScopLiasseFiscaleSource(models.Model):
    _name = "scop.liasse.fiscale.source"
    _description = "Source de Liasse Fiscale"

    name = fields.Char('Source de Liasse Fiscale')
    id_riga = fields.Integer("ID RIGA")
