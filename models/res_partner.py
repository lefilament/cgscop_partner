# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from datetime import datetime, timedelta
from odoo import models, fields, api
from odoo.exceptions import ValidationError


class ScopPartner(models.Model):
    _inherit = "res.partner"

    def _default_ur(self):
        return self.env['res.company']._ur_default_get()

    def _default_country(self):
        return self.env.ref('base.fr', False)

    # Infos générales - champs non affichés
    is_cooperative = fields.Boolean("Est une coopérative")
    current_user_ur_id = fields.Many2one(
        'union.regionale',
        string="Union Régionale de l'utilisateur",
        compute='_compute_current_user_ur_id',
        search='_search_current_user_ur_id')

    # Informations organisme - champs non affichés
    id_ur = fields.Char("Identifiant UR")
    id_riga = fields.Char("Identifiant RIGA")

    # Informations Bandeau
    project_status = fields.Selection(
        [('1_information', "Phase d'information"),
         ('2_pre-diagnostic', 'Phase de pré-diagnostic'),
         ('3_accompagnement', "Phase d'accompagnement projet"),
         ('4_adhesion', "Phase d'adhésion"),
         ('5_cg', 'Soumis CGScop'),
         ('6_suivi', 'Phase de suivi'),
         ('7_abandonne', 'Projet abandonné')],
        track_visibility='onchange',
        string='Statut projet',
        index=True)
    name = fields.Char(index=True, track_visibility='onchange')
    cooperative_form_id = fields.Many2one(
        'res.partner.cooperative.form',
        string="Cooperative form",
        on_delete='restrict',
        track_visibility='onchange')
    partner_company_type_id = fields.Many2one(
        comodel_name='res.partner.company.type',
        string='Legal Form',
        on_delete='restrict',
        track_visibility='onchange')
    membership_status = fields.Selection(
        [('member', 'Adhérent'),
         ('not_member', 'Non Adhérent'),
         ('out', 'Radié')],
        string="Statut d'adhésion",
        compute='_compute_membership', store=True)
    member_number = fields.Char(
        "No adhérent (texte)",
        compute='_compute_membership', store=True)
    member_number_int = fields.Integer(
        "No adhérent",
        compute='_compute_membership_int', store=True, group_operator="")
    cae = fields.Boolean("CAE", track_visibility='onchange')

    dissolution_date = fields.Date('Date de Décès',
                                   track_visibility='onchange')
    dissolution_reason_id = fields.Many2one(
        'res.partner.dissolution.reason',
        string="Motif Décès",
        on_delete='restrict',
        track_visibility='onchange')

    # Infos générales / Contact
    sigle = fields.Char("Sigle")
    street = fields.Char("Rue", track_visibility='onchange')
    street2 = fields.Char("Rue 2", track_visibility='onchange')
    street3 = fields.Char("Rue 3", track_visibility='onchange')
    zip = fields.Char("CP", change_default=True, track_visibility='onchange')
    zip_departement = fields.Char(
        "Num Département", 
        compute='_compute_num_departement',
        store=True)
    region = fields.Many2one(
        comodel_name='res.country.state',
        string='Région',
        compute='_compute_region',
        store=True)
    city = fields.Char("Ville", track_visibility='onchange')
    cedex = fields.Char("Cedex", track_visibility='onchange')
    country_id = fields.Many2one('res.country', string='Country',
                                 ondelete='restrict',
                                 default=_default_country,
                                 track_visibility='onchange')
    phone = fields.Char("Téléphone 1", track_visibility='onchange')
    mobile = fields.Char("Téléphone 2", track_visibility='onchange')
    email = fields.Char("eMail administratif", track_visibility='onchange')
    facebook = fields.Char("Facebook")
    linkedin = fields.Char("LinkedIn")
    twitter = fields.Char("Twitter")

    # Infos générales / Suivi UR
    ur_id = fields.Many2one(
        'union.regionale',
        string='Union Régionale',
        index=True,
        on_delete='restrict',
        default=_default_ur,
        track_visibility='onchange')
    creation_delegate_id = fields.Many2one(
        'res.users',
        string='Délégué de création',
        on_delete='restrict', track_visibility='onchange')
    followup_delegate_id = fields.Many2one(
        'res.users',
        string='Délégué de suivi',
        domain=[('active', '=', True)],
        on_delete='restrict',
        track_visibility='onchange')
    segment_1_id = fields.Many2many('res.partner.segment1',
                                    column1='partner_id',
                                    column2='segment_id',
                                    string='Segmentation 1')
    segment_2_id = fields.Many2many('res.partner.segment2',
                                    column1='partner_id',
                                    column2='segment_id',
                                    string='Segmentation 2')
    segment_3_id = fields.Many2many('res.partner.segment3',
                                    column1='partner_id',
                                    column2='segment_id',
                                    string='Segmentation 3')
    segment_4_id = fields.Many2many('res.partner.segment4',
                                    column1='partner_id',
                                    column2='segment_id',
                                    string='Segmentation 4')

    # Infos générales / Infos activité
    creation_origin_id = fields.Many2one(
        'res.partner.creation.origin',
        string="Origine création en coop",
        domain=[('parent_id', '=', False)],
        on_delete='restrict', track_visibility='onchange')
    creation_suborigin_id = fields.Many2one(
        'res.partner.creation.origin',
        string="Sous-Origine création en coop",
        domain=[('child_ids', '=', False)],
        on_delete='restrict', track_visibility='onchange')
    date_1st_sign = fields.Date(
        'Date 1er signature coop',
        track_visibility='onchange')
    registration_date = fields.Date(
        string="Date d'immatriculation RCS",
        track_visibility='onchange')
    social_object = fields.Text('Objet Social', track_visibility='onchange')
    naf_id = fields.Many2one(
        'res.partner.naf',
        string='Code NAF',
        on_delete='restrict',
        track_visibility='onchange')
    certification_ids = fields.Many2many(
        comodel_name='res.partner.certification',
        string='Agrément', on_delete='restrict')
    siret = fields.Char(string='SIRET', size=14, index=True,
                        track_visibility='onchange')
    formatted_siret = fields.Char(string='SIRET formaté',
                                  compute='_compute_from_siret')
    siren = fields.Char(string='SIREN', size=11, compute='_compute_from_siret')
    capital = fields.Integer("Capital (en €)")
    first_closeout = fields.Date(
        '1er bilan en coop')
    closeout_month = fields.Selection(
        [(12842, 'Janvier'),
         (12843, 'Février'),
         (12844, 'Mars'),
         (12845, 'Avril'),
         (12846, 'Mai'),
         (12847, 'Juin'),
         (12848, 'Juillet'),
         (12849, 'Août'),
         (12850, 'Septembre'),
         (12851, 'Octobre'),
         (12852, 'Novembre'),
         (12853, 'Décembre')],
        string='Mois clôture exercices', default=12853)
    is_seed_scop = fields.Boolean("Est une SCOP d'amorçage")
    seed_end = fields.Date("Date de fin du dispositif d'amorçage")
    is_incubated = fields.Boolean("Est incubé")
    incubator_id = fields.Many2one(
        'res.partner',
        string='Incubateur',
        domain=[('active', '=', True), ('is_company', '=', True),
                ('organization_subtype_id.name', '=', 'Incubateur')],
        on_delete='restrict')

    # Contacts
    director_ids = fields.One2many(
        'res.partner', 'parent_id',
        string='Contacts Dirigeants',
        domain=[('active', '=', True), ('mandate_id', '!=', False)])
    other_child_ids = fields.One2many(
        'res.partner', 'parent_id', string='Autres Contacts',
        domain=[('active', '=', True), ('mandate_id', '=', False)])

    # Révisions
    revision_contract = fields.Date("Date du contrat de révision",
                                    track_visibility='onchange')
    revision_company_id = fields.Many2one(
        'res.partner',
        string='Organisme de révision',
        domain=[('active', '=', True), ('is_company', '=', True),
                '|', ('organization_subtype_id.name', '=',
                      'Organisme réviseur'),
                ('organization_subtype_id.name', '=', 'Organisme réviseur, ' +
                 'Expert-comptable / Commissaire au compte')],
        on_delete='restrict',
        track_visibility='onchange')
    revision_backup_company_id = fields.Many2one(
        'res.partner',
        string='Organisme de révision suppléant',
        domain=[('active', '=', True), ('is_company', '=', True),
                '|', ('organization_subtype_id.name', '=',
                      'Organisme réviseur'),
                ('organization_subtype_id.name', '=', 'Organisme réviseur, ' +
                 'Expert-comptable / Commissaire au compte')],
        on_delete='restrict')
    revision_person_id = fields.Many2one(
        'res.users',
        string='Réviseur',
        on_delete='restrict', 
        track_visibility='onchange')
    revision_certified_person_id = fields.Many2one(
        'res.users',
        string='Réviseur agréé',
        on_delete='restrict')
    revision_person_assign_date = fields.Date("Date de nomination du réviseur",
                                              track_visibility='onchange')
    revision_type = fields.Selection(
        [('1y', "Annuelle"),
         ('5y', 'Quinquennale'),
         ('5ys', "Quinquennale séquencée")],
        string='Périodicité de la révision')
    revision_next_date = fields.Date("Prochain exercice révisable")
    revision_format_id = fields.Many2one(
        'scop.revision.format',
        string='Format de révision',
        on_delete='restrict')

    revision_ids = fields.One2many(
        comodel_name='scop.revision',
        inverse_name='partner_id',
        string='Liste des Révisions')

    # Historique
    scop_period_ids = fields.One2many(
        comodel_name='scop.period',
        inverse_name='partner_id',
        string="Historique")

    # Période d'adhésion
    membership_period_ids = fields.One2many(
        comodel_name='scop.membership.period',
        inverse_name='partner_id',
        string="Périodes d'adhésion")

    # Cotisation
    contribution_ids = fields.One2many(
        comodel_name='scop.contribution',
        inverse_name='partner_id',
        string='Cotisations')
    is_up_to_date_cg = fields.Boolean("A jour cotisation CG")
    is_up_to_date_fede = fields.Boolean("A jour cotisation Fédérations")
    is_up_to_date_ur = fields.Boolean("A jour cotisation UR")

    # Questionnaire
    # Liste Ministère
    direccte_id = fields.Many2one(
        'res.partner',
        string='DIRECCTE rattachée',
        domain=[('active', '=', True), ('is_company', '=', True),
                ('organization_subtype_id', '=', 'DIRECCTE')],
        on_delete='restrict')
    questionnaire_ids = fields.One2many(
        comodel_name='scop.questionnaire',
        inverse_name='partner_id',
        string='Listes Ministère')
    staff_last = fields.Integer(
        string="Dernier effectif connu",
        compute="_compute_last_effective",
        store=True)
    staff_last_date = fields.Date(
        string="Date dernier effectif connu",
        compute="_compute_last_effective",
        store=True)

    # Liasse fiscale
    liasse_fiscale_ids = fields.One2many(
        comodel_name='scop.liasse.fiscale',
        inverse_name='partner_id',
        string='Liasses Fiscales')

    # Champs pour personnes
    birthyear = fields.Integer("Année de naissance")
    subscription_ids = fields.One2many(
        comodel_name='res.partner.newsletter.subscription',
        inverse_name='partner_id',
        string='Abonnements')
    contact_origin_id = fields.Many2one(
        'res.partner.rgpd.origin',
        string="Origine du contact",
        on_delete='restrict')
    mandate_id = fields.Many2one(
        'res.partner.mandate',
        string="Mandat",
        on_delete='restrict')
    contact_legality = fields.Selection(
        [("employee", "Salarié"),
         ("customer", "Client en contrat"),
         ("supplier", "Fournisseur en contrat"),
         ("consent", "Consentement"),
         ("legitimate", "Intérêt légitime"),
         ("none", "Aucune")],
        string="Licéité du contact",
        compute="_compute_contact_legality",
        store=True)

    # Champs pour partenaires
    organization_type_id = fields.Many2one(
        'res.partner.organization.type',
        string="Catégorie Type",
        domain=[('parent_id', '=', False)],
        on_delete='restrict',
        track_visibility='onchange')
    organization_subtype_id = fields.Many2one(
        'res.partner.organization.type',
        string="Type",
        domain=[('child_ids', '=', False)],
        on_delete='restrict',
        track_visibility='onchange')

    # ------------------------------------------------------
    # Constrains
    # ------------------------------------------------------
    @api.constrains('zip_id', 'country_id', 'city_id', 'state_id')
    def _check_zip(self):
        return

    @api.constrains('siret')
    def _check_siret(self):
        if self.siret and not self.env.context.get('import_file'):
            siren = (self.siret[:3] + " " + 
                     self.siret[3:6] + " " +
                     self.siret[6:9])
            if not self.siret.isdigit():
                raise ValidationError(
                    "Ce SIRET n'est pas valide")
            if len(self.siret) != 14:
                raise ValidationError(
                    "La longueur du SIRET doit être égale à 14")
            if (self.search_count([('siret', '=like', siren+"%"),
                                   ('is_cooperative', "=", True)]) > 1):
                raise ValidationError(
                    "Ce SIREN existe déjà parmi les coopératives")
            elif self.search_count([('siret', '=', self.siret)]) > 1:
                raise ValidationError("Ce SIRET existe déjà")

    # ------------------------------------------------------
    # Actions
    # ------------------------------------------------------
    @api.multi
    def open_facebook(self):
        self.ensure_one()
        return {
            "type": "ir.actions.act_url",
            "url": self.facebook,
        }

    @api.multi
    def open_linkedin(self):
        self.ensure_one()
        return {
            "type": "ir.actions.act_url",
            "url": self.linkedin,
        }

    @api.multi
    def open_twitter(self):
        self.ensure_one()
        return {
            "type": "ir.actions.act_url",
            "url": self.twitter,
        }

    # ------------------------------------------------------
    # Onchange
    # ------------------------------------------------------
    @api.onchange('creation_origin_id')
    def onchange_creation_origin_id(self):
        for coop in self:
            coop.creation_suborigin_id = False

    @api.onchange('is_seed_scop')
    def onchange_is_seed_scop(self):
        for coop in self:
            if coop.is_seed_scop == True:
                if coop.date_1st_sign:
                    coop.seed_end = coop.date_1st_sign + timedelta(2556)
                else:
                    coop.seed_end = datetime.today().date() + timedelta(2556)
            else:
                coop.seed_end = False

    @api.onchange('organization_subtype_id')
    def onchange_organization_subtype_id(self):
        for partner in self:
            if partner.organization_subtype_id:
                partner.organization_type_id = \
                    partner.organization_subtype_id.parent_id
            else:
                partner.organization_type_id = False

    @api.onchange('name')
    def onchange_name(self):
        if self.search_count([('name', '=ilike', self.name)]) > 0:
            return {
                'warning': {
                    'title': "Attention",
                    'message': "Ce nom / cette raison sociale existe déjà, "
                               + "merci de vérifier que vous n'êtes pas en "
                               + "train de créer un doublon"}}

    @api.onchange('mobile')
    def onchange_mobile(self):
        if self.mobile and len(self.mobile) > 0 and len(self.mobile) < 10:
            raise ValidationError(
                "Le numéro de téléphone doit contenir au moins 10 caractères")

    @api.onchange('phone')
    def onchange_phone(self):
        if self.phone and len(self.phone) > 0 and len(self.phone) < 10:
            raise ValidationError(
                "Le numéro de téléphone doit contenir au moins 10 caractères")

    @api.onchange('siret')
    def onchange_siret(self):
        if self.siret:
            siren = (self.siret[:3] + " " + 
                     self.siret[3:6] + " " +
                     self.siret[6:9])
            if self.search_count([('siret', '=like', self.siret)]) > 0:
                return {
                    'warning': {
                        'title': "Attention",
                        'message': "Ce SIRET existe déjà, merci de vérifier "
                                   + "que vous n'êtes pas en train de créer un"
                                   + " doublon"}}
            elif self.search_count([('siret', '=like', siren+"%")]) > 0:
                return {
                    'warning': {
                        'title': "Attention",
                        'message': "Ce SIREN existe déjà, merci de vérifier "
                                   + "que vous n'êtes pas en train de créer un"
                                   + " doublon"}}

    # ------------------------------------------------------
    # Common functions
    # ------------------------------------------------------
    def _create_period(self, partner):
        new_period = self.env['scop.period'].create({
            'partner_id': partner.id,
            'start': partner.registration_date or fields.Date.today(),
            'name': partner.name,
            'cooperative_form_id': partner.cooperative_form_id.id,
            'partner_company_type_id': partner.partner_company_type_id.id,
            'siret': partner.siret,
            'street': partner.street,
            'street2': partner.street2,
            'street3': partner.street3,
            'zip': partner.zip,
            'zip_id': partner.zip_id.id,
            'city': partner.city,
            'cedex': partner.cedex,
            'state_id': partner.state_id.id,
            'country_id': partner.country_id.id,
            'naf_id': partner.naf_id.id,
            'cae': partner.cae
        })
        partner.scop_period_ids = new_period

    # ------------------------------------------------------
    # Override ORM
    # ------------------------------------------------------
    # Creation d'une periode lorsque le statut passe en Phase de Suivi
    @api.multi
    def write(self, vals):
        if vals.get('name'):
            vals['name'] = vals.get('name').title()
        if vals.get('lastname'):
            vals['lastname'] = vals.get('lastname').title()
        if vals.get('firstname'):
            vals['firstname'] = vals.get('firstname').title()
        if vals.get('city'):
            vals['city'] = vals.get('city').upper()
        result = super(ScopPartner, self).write(vals)
        for partner in self:
            if (not partner.is_company
                    and partner.type == 'contact'
                    and not partner.user_ids):
                if (not partner.email and not partner.phone
                        and not partner.mobile):
                    raise ValidationError(
                        "Vous devez saisir au moins un e-mail ou un téléphone\
                        pour " + partner.name)

            if (vals.get('project_status') == '6_suivi'
                    and not self.env.context.get('import_file')):
                partner._create_period(partner)
            partners_to_subscribe = [
                partner.followup_delegate_id.partner_id.id,
                partner.creation_delegate_id.partner_id.id
            ]
            partner.message_subscribe(partner_ids=partners_to_subscribe)
        return result

    # Création d'une période lors de la création d'une coopérative
    @api.model_create_multi
    def create(self, vals_list):
        for vals in vals_list:       
            if vals.get('name'):
                vals['name'] = vals.get('name').title()
            if vals.get('lastname'):
                vals['lastname'] = vals.get('lastname').title()
            if vals.get('firstname'):
                vals['firstname'] = vals.get('firstname').title()
            if vals.get('city'):
                vals['city'] = vals.get('city').upper()
        partners = super(ScopPartner, self).create(vals_list)
        for vals in vals_list:
            if vals.get('is_cooperative') and vals.get(
                    'project_status') == '6_suivi':
                for partner in partners:
                    partner._create_period(partner)

        for partner in partners:
            partners_to_subscribe = [
                partner.followup_delegate_id.partner_id.id,
                partner.creation_delegate_id.partner_id.id
            ]
            partner.message_subscribe(partner_ids=partners_to_subscribe)
        return partners

    # ------------------------------------------------------
    # Computed Fields
    # ------------------------------------------------------
    @api.depends('siret')
    def _compute_from_siret(self):
        for company in self:
            if company.siret:
                company.siren = (company.siret[:3] + " " +
                                 company.siret[3:6] + " " +
                                 company.siret[6:9])
                company.formatted_siret = (company.siret[:3] + " " +
                                           company.siret[3:6] + " " +
                                           company.siret[6:9] + " " +
                                           company.siret[9:])

    @api.depends('zip')
    def _compute_num_departement(self):
        for company in self:
            if company.zip:
                company.zip_departement = company.zip[:2]

    @api.depends('zip')
    def _compute_region(self):
        for partner in self:
            if partner.zip:
                zip_id = self.env['res.city.zip'].search([
                    ('name', '=', partner.zip)])
                if zip_id:
                    partner.region = zip_id[0].city_id[0].state_id

    @api.model
    def _compute_current_user_ur_id(self):
        for partner in self:
            partner.current_user_ur_id = self.env.user.company_id.ur_id.id

    def _search_current_user_ur_id(self, operator, value):
        return [('ur_id', '=', self.env.user.company_id.ur_id.id)]

    @api.depends('contact_origin_id',
                 'parent_id.cooperative_form_id',
                 'parent_id.membership_status')
    def _compute_contact_legality(self):
        for partner in self:
            partner.contact_legality = 'none'
            if (partner.contact_origin_id.name ==
                    "Fiche contact, site internet"):
                partner.contact_legality = 'consent'
            if partner.contact_origin_id.name in (
                    "Prospect journée d'info coll",
                    "Prospect (salon, rdv, internet…)",
                    "Elus"):
                partner.contact_legality = 'legitimate'
            if (partner.contact_origin_id.name in (
                    "Salariés CG", "Salariés UR", "Salariés Fédération")):
                partner.contact_legality = 'employee'
            if (partner.contact_origin_id.name in (
                    "Elus", "VIP, Officiels", "Fournisseurs")):
                partner.contact_legality = 'legitimate'
            if not partner.is_company and partner.parent_id:
                parent = partner.parent_id
                if (partner.contact_origin_id.name in (
                        "Dossiers d'adhésion",
                        "Dossiers annuels non LM (scic, scop47)")
                        and parent.cooperative_form_id
                        and parent.membership_status == "member"):
                    partner.contact_legality = 'customer'
                if (partner.contact_origin_id.name == (
                        "Dossiers annuels non LM (scic, scop47)")
                        and parent.membership_status != "member"):
                    partner.contact_legality = 'legitimate'
                if (partner.contact_origin_id.name == (
                        "Dossiers Liste ministère")
                        and parent.cooperative_form_id.name == "SCIC"):
                    partner.contact_legality = 'customer'

    @api.depends('membership_period_ids', 'membership_period_ids.state',
                 'membership_period_ids.end_reason_id')
    @api.multi
    def _compute_membership(self):
        for partner in self:
            type_cg = self.env['scop.membership.type'].search([
                ('name', '=', 'Confédération générale des SCOP')], limit=1).id
            last_membership_period = self.env['scop.membership.period'].search(
                [('partner_id', '=', partner.id),
                 ('type_id', '=', type_cg)],
                limit=1,
                order='start desc')
            if (last_membership_period
                    and not last_membership_period.end_reason_id
                    and last_membership_period.state == 'done'):
                partner.membership_status = "member"
                partner.member_number = last_membership_period.number
            elif (last_membership_period
                  and last_membership_period.end_reason_id):
                partner.membership_status = "out"
                partner.member_number = last_membership_period.number
            else:
                partner.membership_status = "not_member"

    @api.depends('member_number')
    @api.multi
    def _compute_membership_int(self):
        for partner in self:
            if partner.member_number:
                partner.member_number_int = int(partner.member_number)

    @api.depends('questionnaire_ids', 'questionnaire_ids.staff_count',
                 'questionnaire_ids.effective_date')
    @api.multi
    def _compute_last_effective(self):
        for partner in self:
            lm = partner.questionnaire_ids.search(
                [['partner_id', '=', partner.id], ['staff_count', '>', 0]],
                limit=1,
                order='effective_date desc')
            if lm:
                partner.staff_last = lm[0].staff_count
                partner.staff_last_date = lm[0].effective_date

    # ------------------------------------------------------
    # Button Action
    # ------------------------------------------------------
    @api.multi
    def scop_send_to_cg(self):
        self.write({
            'project_status': '5_cg',
        })

        return True

    @api.multi
    def partner_archive(self):
        self.active = False

    # TODO - remove temporary function once RIGA API in place
    @api.multi
    def scop_valid_cg(self):
        self.write({
            'project_status': '6_suivi',
        })
        return True

    @api.multi
    def scop_abandonne(self):
        self.write({
            'project_status': '7_abandonne',
        })
        return True


class ResPartneCertification(models.Model):
    _name = "res.partner.certification"
    _description = "Certification"

    name = fields.Char('Certification')
    id_riga = fields.Integer("ID RIGA")
    color = fields.Integer('Couleur Index')


class ScopPartnerCompanyType(models.Model):
    _inherit = "res.partner.company.type"

    id_riga = fields.Integer("ID RIGA")
    is_coop = fields.Boolean("Est un statut coopératif")


class ResPartnerCooperativeForm(models.Model):
    _name = "res.partner.cooperative.form"
    _description = "Cooperative form"

    name = fields.Char('Cooperative form')
    id_riga = fields.Integer("ID RIGA")


class ResPartnerCreationOrigin(models.Model):
    _name = "res.partner.creation.origin"
    _description = "Creation Origin"
    _parent_name = "parent_id"
    _parent_store = True
    _rec_name = 'name'
    _order = 'id'

    name = fields.Char('Origine / Sous-Origine', index=True, required=True)
    parent_id = fields.Many2one(
        comodel_name='res.partner.creation.origin',
        string='Origine (parent)',
        index=True,
        ondelete='restrict')
    parent_path = fields.Char(index=True)
    child_ids = fields.One2many(
        comodel_name='res.partner.creation.origin',
        inverse_name='parent_id',
        string='Sous origines')
    id_riga = fields.Integer("ID RIGA")

    @api.constrains('parent_id')
    def _check_origin_recursion(self):
        if not self._check_recursion():
            raise ValidationError(
                'Vous ne pouvez pas créer des origines récursives.')
        return True


class ResPartnerDissolutionReason(models.Model):
    _name = "res.partner.dissolution.reason"
    _description = "Dissolution Reason"

    name = fields.Char('Dissolution Reason')


class ResPartnerMandate(models.Model):
    _name = "res.partner.mandate"
    _description = "Mandat"

    name = fields.Char('Mandat')


class ScopPartnerNaf(models.Model):
    _inherit = "res.partner.naf"

    id_riga = fields.Integer("ID RIGA")


class ResPartnerOrganizationType(models.Model):
    _name = "res.partner.organization.type"
    _description = "Type of Organization"
    _parent_name = "parent_id"
    _parent_store = True
    _rec_name = 'name'
    _order = 'id'

    name = fields.Char('Type / Sous-Type', index=True, required=True)
    parent_id = fields.Many2one(
        comodel_name='res.partner.organization.type',
        string='Type (parent)',
        index=True,
        ondelete='cascade')
    parent_path = fields.Char(index=True)
    child_ids = fields.One2many(
        comodel_name='res.partner.organization.type',
        inverse_name='parent_id',
        string='Sous types')
    id_riga = fields.Integer("ID RIGA")

    @api.constrains('parent_id')
    def _check_origin_recursion(self):
        if not self._check_recursion():
            raise ValidationError(
                'Vous ne pouvez pas créer des types récursifs.')
        return True


class ResPartnerRgpdOrigin(models.Model):
    _name = "res.partner.rgpd.origin"
    _description = "Origin"

    name = fields.Char('Origin')


class ResPartnerSegment1(models.Model):
    _name = "res.partner.segment1"
    _description = "Segments 1"
    _rec_name = 'name'
    _order = 'ur_id, name'

    def _default_ur(self):
        return self.env['res.company']._ur_default_get()

    name = fields.Char('Nom', index=True, required=True)
    color = fields.Integer(string='ID Couleur')
    id_riga = fields.Integer("ID RIGA")
    ur_id = fields.Many2one(
        'union.regionale',
        string='Union Régionale',
        index=True,
        on_delete='restrict',
        default=_default_ur)


class ResPartnerSegment2(models.Model):
    _name = "res.partner.segment2"
    _description = "Segments 2"
    _rec_name = 'name'
    _order = 'ur_id, name'

    def _default_ur(self):
        return self.env['res.company']._ur_default_get()

    name = fields.Char('Nom', index=True, required=True)
    color = fields.Integer(string='ID Couleur')
    id_riga = fields.Integer("ID RIGA")
    ur_id = fields.Many2one(
        'union.regionale',
        string='Union Régionale',
        index=True,
        on_delete='restrict',
        default=_default_ur)


class ResPartnerSegment3(models.Model):
    _name = "res.partner.segment3"
    _description = "Segments 3"
    _rec_name = 'name'
    _order = 'ur_id, name'

    def _default_ur(self):
        return self.env['res.company']._ur_default_get()

    name = fields.Char('Nom', index=True, required=True)
    color = fields.Integer(string='ID Couleur')
    id_riga = fields.Integer("ID RIGA")
    ur_id = fields.Many2one(
        'union.regionale',
        string='Union Régionale',
        index=True,
        on_delete='restrict',
        default=_default_ur)


class ResPartnerSegment4(models.Model):
    _name = "res.partner.segment4"
    _description = "Segments 4"
    _rec_name = 'name'
    _order = 'ur_id, name'

    def _default_ur(self):
        return self.env['res.company']._ur_default_get()

    name = fields.Char('Nom', index=True, required=True)
    color = fields.Integer(string='ID Couleur')
    id_riga = fields.Integer("ID RIGA")
    ur_id = fields.Many2one(
        'union.regionale',
        string='Union Régionale',
        index=True,
        on_delete='restrict',
        default=_default_ur)
