odoo.define('cgscop_partner.kanban_renderer',function(require){
"use strict";


var KanbanRenderer = require('web.KanbanRenderer');

KanbanRenderer.include({

    _setState: function (state) {
        this._super.apply(this, arguments);

        var arch = this.arch;
        if (arch.attrs.block_drag_drop_kanban) {
            if (arch.attrs.block_drag_drop_kanban=='true') {
                this.columnOptions.draggable = false;
            }
        }

    },

});

});