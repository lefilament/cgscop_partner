odoo.define('cgscop_partner.relational_fields',function(require){
"use strict";

var AbstractField = require('web.AbstractField');
var relational_fields = require('web.relational_fields');
var FieldStatus = relational_fields.FieldStatus;

var core = require('web.core');

var qweb = core.qweb;

FieldStatus.include({
    
    _render: function () {
        var clickable_attr;
        if (this.mode === 'readonly') {
            clickable_attr = false;
        } else {
            clickable_attr = true;
        }
        var selections = _.partition(this.status_information, function (info) {
            return (info.selected || !info.fold);
        });
        this.$el.html(qweb.render("FieldStatus.content", {
            selection_unfolded: selections[0],
            selection_folded: selections[1],
            clickable: clickable_attr,
        }));
    },

});

});