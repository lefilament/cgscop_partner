# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import api, fields, models
from odoo.exceptions import ValidationError


class ScopMembershipOutWizard(models.TransientModel):
    _name = 'scop.membership.out.wizard'
    _description = "Radiation"

    # Default functions
    @api.model
    def _default_partner_id(self):
        return self.env.context.get('active_id')

    partner_id = fields.Integer('Partner', default=_default_partner_id)
    type_id = fields.Many2one(
        'scop.membership.type',
        string="Type d'adhésion",
        ondelete='restrict', required=True)
    end = fields.Date(
        "Fin d'adhésion", required=True, default=fields.Date.today())
    end_reason_id = fields.Many2one(
        'scop.membership.reason.end',
        string="Motif de fin d’adhésion",
        on_delete='restrict',
        required=True)
    note = fields.Text('Commentaires')

    @api.multi
    def member_out(self):
        # Close previous period
        for period in self:
            last_period = self.env['scop.membership.period'].search(
                [('partner_id', '=', period.partner_id),
                 ('end', '=', False), ('type_id', '=', period.type_id.id)],
                limit=1)
            if last_period:
                if period.end >= last_period.start:
                    last_period.write({
                        'end': period.end,
                        'end_reason_id': period.end_reason_id.id,
                        'note': period.note,
                    })
                else:
                    raise ValidationError(
                        "La date de fin doit être postèrieure à la date de "
                        + "début de la dernière période: "
                        + str(last_period.start))
            else:
                raise ValidationError(
                    "Il n'existe pas de période avec ce type à fermer.")
