# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import api, fields, models
from odoo.exceptions import ValidationError


class ScopPeriodWizard(models.TransientModel):
    _name = 'scop.period.wizard'
    _description = "Création d'une nouvelle période"

    # Default functions
    @api.model
    def _default_partner_id(self):
        return self.env.context.get('active_id')

    @api.model
    def _default_previous_period_id(self):
        return self.env['scop.period'].search(
            [('partner_id', '=', self.env.context.get('active_id')),
             ('end_reason', '=', False)],
            limit=1).id

    @api.model
    def _default_name(self):
        return self.env['res.partner'].browse(
            self.env.context.get('active_id')).name

    @api.model
    def _default_cooperative_form_id(self):
        return self.env['res.partner'].browse(
            self.env.context.get('active_id')).cooperative_form_id

    @api.model
    def _default_partner_company_type_id(self):
        return self.env['res.partner'].browse(
            self.env.context.get('active_id')).partner_company_type_id

    @api.model
    def _default_siret(self):
        return self.env['res.partner'].browse(
            self.env.context.get('active_id')).siret

    @api.model
    def _default_street(self):
        return self.env['res.partner'].browse(
            self.env.context.get('active_id')).street

    @api.model
    def _default_street2(self):
        return self.env['res.partner'].browse(
            self.env.context.get('active_id')).street2

    @api.model
    def _default_street3(self):
        return self.env['res.partner'].browse(
            self.env.context.get('active_id')).street3

    @api.model
    def _default_zip(self):
        return self.env['res.partner'].browse(
            self.env.context.get('active_id')).zip

    @api.model
    def _default_zip_id(self):
        return self.env['res.partner'].browse(
            self.env.context.get('active_id')).zip_id

    @api.model
    def _default_city(self):
        return self.env['res.partner'].browse(
            self.env.context.get('active_id')).city

    @api.model
    def _default_cedex(self):
        return self.env['res.partner'].browse(
            self.env.context.get('active_id')).cedex

    @api.model
    def _default_state_id(self):
        return self.env['res.partner'].browse(
            self.env.context.get('active_id')).state_id

    @api.model
    def _default_country_id(self):
        return self.env['res.partner'].browse(
            self.env.context.get('active_id')).country_id

    @api.model
    def _default_naf_id(self):
        return self.env['res.partner'].browse(
            self.env.context.get('active_id')).naf_id

    @api.model
    def _default_cae(self):
        return self.env['res.partner'].browse(
            self.env.context.get('active_id')).cae

    # Fields common
    partner_id = fields.Integer('Partner', default=_default_partner_id)

    # Fields previous period
    previous_period_id = fields.Integer(default=_default_previous_period_id)
    end_reason = fields.Selection(
        [('juri', "Modification de la forme juridique"),
         ('form', "Changement de forme coopérative"),
         ('acti', "Changement d'activité (NAF)"),
         ('adr', "Changement d'adresse"),
         ('nom', "Changement de dénomination sociale"),
         ('autr', "Autres")],
        string='Motif')
    comments = fields.Text('Commentaires')

    # Fields new period
    start = fields.Date('Début de validité', required=True,
                        default=fields.Date.today())
    name = fields.Char('Raison Sociale', required=True, default=_default_name)
    cooperative_form_id = fields.Many2one(
        'res.partner.cooperative.form',
        string="Cooperative form",
        on_delete='restrict', required=True,
        default=_default_cooperative_form_id)
    partner_company_type_id = fields.Many2one(
        comodel_name='res.partner.company.type',
        string='Legal Form',
        track_visibility='onchange',
        on_delete='restrict', required=True,
        default=_default_partner_company_type_id)
    siret = fields.Char(string='SIRET', size=14, required=True,
                        default=_default_siret)
    street = fields.Char(default=_default_street)
    street2 = fields.Char(default=_default_street2)
    street3 = fields.Char(default=_default_street3)
    zip = fields.Char(default=_default_zip)
    zip_id = fields.Many2one('res.city.zip', 'ZIP Location',
                             default=_default_zip_id)
    city = fields.Char(default=_default_city)
    cedex = fields.Char(default=_default_cedex)
    state_id = fields.Many2one(
        "res.country.state",
        string='State',
        ondelete='restrict',
        domain="[('country_id', '=?', country_id)]",
        default=_default_state_id)
    country_id = fields.Many2one(
        'res.country', string='Country', ondelete='restrict',
        default=_default_country_id)
    naf_id = fields.Many2one(
        'res.partner.naf',
        string='Code NAF',
        on_delete='restrict', default=_default_naf_id)
    cae = fields.Boolean("CAE", default=_default_cae)


    # Action
    @api.onchange('zip_id')
    def _onchange_zip_id(self):
        if self.zip_id:
            self.zip = self.zip_id.name
            self.city = self.zip_id.city_id.name
            self.country_id = self.zip_id.city_id.country_id
            self.state_id = self.zip_id.city_id.state_id

    @api.multi
    def create_period(self):
        for period in self:
            partner_values = {}
            # Close previous period
            previous_period = self.env['scop.period'].search(
                [('partner_id', '=', self.env.context.get('active_id'))],
                limit=1)
            if previous_period:
                if previous_period.end:
                    if period.start >= previous_period.end:
                        partner_values = {
                            'dissolution_date': False,
                        }
                    else:
                        raise ValidationError(
                            "La nouvelle période ne peut commencer avant la "
                            + "fin de la période précédente : "
                            + str(previous_period.end))
                if period.start >= previous_period.start:
                    if not previous_period.end_reason:
                        previous_period.write({
                            'end': period.start,
                            'end_reason': period.end_reason,
                            'comments': period.comments,
                        })
                else:
                    raise ValidationError(
                        "La nouvelle période ne peut commencer avant la "
                        + "période précédente : "
                        + str(previous_period.start))

            # Create new period
            period_values = {
                'start': period.start,
                'partner_id': period.partner_id
            }

            values = {
                'name': period.name,
                'dissolution_reason_id': False,
                'cooperative_form_id': period.cooperative_form_id.id,
                'partner_company_type_id': period.partner_company_type_id.id,
                'siret': period.siret,
                'street': period.street,
                'street2': period.street2,
                'street3': period.street3,
                'zip': period.zip,
                'zip_id': period.zip_id.id,
                'city': period.city,
                'cedex': period.cedex,
                'state_id': period.state_id.id,
                'country_id': period.country_id.id,
                'naf_id': period.naf_id.id,
                'cae': period.cae
            }
            period_values.update(values)
            self.env['scop.period'].create(period_values)

            # Update partner
            partner_values.update(values)
            partner = self.env['res.partner'].browse(period.partner_id)
            partner.write(partner_values)
