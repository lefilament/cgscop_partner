# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import api, fields, models
from odoo.exceptions import ValidationError


class ScopMembershipPeriodWizard(models.TransientModel):
    _name = 'scop.membership.period.wizard'
    _description = "Création d'une nouvelle période d'adhésion"

    # Default functions
    @api.model
    def _default_partner_id(self):
        return self.env.context.get('active_id')

    partner_id = fields.Integer('Partner', default=_default_partner_id)
    type_id = fields.Many2one(
        'scop.membership.type',
        string="Type d'adhésion",
        ondelete='restrict', required=True)
    start = fields.Date('Début d’adhésion', required=True,
                        default=fields.Date.today())
    number = fields.Char("No adhérent", required=True)
    state = fields.Selection(
        [("none", "Non Adhérent"),
         ("ongoing", "Traitement de l'adhésion"),
         ("approval", "Attente d'approbation"),
         ("done", "Adhérent")],
        string="Statut adhésion", default="none", required=True)
    note = fields.Text('Commentaires')

    @api.multi
    def create_period(self):
        for period in self:
            # Raise validation error if previous period exists
            if self.env['scop.membership.period'].search(
                    [('partner_id', '=', period.partner_id),
                     ('end', '=', False), ('type_id', '=', period.type_id.id)],
                    limit=1):
                raise ValidationError(
                    "Une période d'adhésion existe déjà pour ce type")
            else:
                self.env['scop.membership.period'].create({
                    'partner_id': period.partner_id,
                    'type_id': period.type_id.id,
                    'start': period.start,
                    'number': period.number,
                    'state': period.state,
                    'note': period.note
                    })
